package mag.ir.musicplayer.Models;

public class Albums {
    String AlbumName;
    String ALbumArtist;
    String AlbumArt;
    long AlbumID;

    public String getAlbumName() {
        return AlbumName;
    }

    public void setAlbumName(String albumName) {
        AlbumName = albumName;
    }

    public String getALbumArtist() {
        return ALbumArtist;
    }

    public void setALbumArtist(String ALbumArtist) {
        this.ALbumArtist = ALbumArtist;
    }

    public String getAlbumArt() {
        return AlbumArt;
    }

    public void setAlbumArt(String albumArt) {
        AlbumArt = albumArt;
    }

    public long getAlbumID() {
        return AlbumID;
    }

    public void setAlbumID(long albumID) {
        AlbumID = albumID;
    }
}
