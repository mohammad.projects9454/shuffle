package mag.ir.musicplayer.Models;

public class Songs {
    private long mSongID;
    private String mSongTitle;
    private String mPath;
    private String mSongArtist;

    public Songs(long id, String title, String artist, String mPath) {
        mSongID = id;
        mSongTitle = title;
        this.mSongArtist = artist;
        this.mPath = mPath;
    }

    public String getmPath() {
        return mPath;
    }

    public String getmSongArtist() {
        return mSongArtist;
    }

    public long getSongID() {
        return mSongID;
    }

    public String getSongTitle() {
        return mSongTitle;
    }
}
