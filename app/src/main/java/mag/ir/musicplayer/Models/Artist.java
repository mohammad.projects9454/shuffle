package mag.ir.musicplayer.Models;

public class Artist {
    String numOfTracks, numOfAlbums, albumArt, name;
    long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNumOfTracks() {
        return numOfTracks;
    }

    public void setNumOfTracks(String numOfTracks) {
        this.numOfTracks = numOfTracks;
    }

    public String getNumOfAlbums() {
        return numOfAlbums;
    }

    public void setNumOfAlbums(String numOfAlbums) {
        this.numOfAlbums = numOfAlbums;
    }

    public String getAlbumArt() {
        return albumArt;
    }

    public void setAlbumArt(String albumArt) {
        this.albumArt = albumArt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
