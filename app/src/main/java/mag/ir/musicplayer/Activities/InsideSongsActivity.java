package mag.ir.musicplayer.Activities;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import mag.ir.musicplayer.Adapters.SongAdapter;
import mag.ir.musicplayer.R;

public class InsideSongsActivity extends AppCompatActivity {

    @BindView(R.id.rec)
    RecyclerView recyclerView;
    @BindView(R.id.back)
    RelativeLayout back;
    private String type, albumID, path, artist;
    private ArrayList<mag.ir.musicplayer.Models.Songs> songs;
    private SongAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inside_songs);
        ButterKnife.bind(this);

        songs = new ArrayList<>();

        type = getIntent().getStringExtra("type");
        if (type.equals("1")) {
            albumID = getIntent().getStringExtra("id");
            getSongsByAlbum(Long.valueOf(albumID));
        } else if (type.equals("2")) {
            path = getIntent().getStringExtra("path");
            getSongsByFolder(path);
        } else if (type.equals("3")) {
            artist = getIntent().getStringExtra("artist");
            getSongsByArtist(artist);
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InsideSongsActivity.super.onBackPressed();
            }
        });

    }

    private void getSongsByAlbum(long albumId) {
        mAdapter = new SongAdapter(songs, this);
        String selection = "is_music != 0";

        if (albumId > 0) {
            selection = selection + " and album_id = " + albumId;
        }

        ContentResolver contentResolver = getContentResolver();
        Uri songUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String orderBy = MediaStore.Audio.Media.TITLE + " ASC";
        Cursor songCursor = contentResolver.query(songUri, null, selection, null, orderBy);

        if (songCursor != null && songCursor.moveToFirst()) {
            int songId = songCursor.getColumnIndex(MediaStore.Audio.Media._ID);
            int songTitle = songCursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int songArtist = songCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
            int column_index = songCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);

            do {

                long currentId = songCursor.getLong(songId);
                String currentTitle = songCursor.getString(songTitle);
                String currentArtist = songCursor.getString(songArtist);
                String pathId = songCursor.getString(column_index);

                songs.add(new mag.ir.musicplayer.Models.Songs(currentId, currentTitle, currentArtist, pathId));
            } while (songCursor.moveToNext());

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(mAdapter);
        }
    }

    private void getSongsByFolder(String path) {
        mAdapter = new SongAdapter(songs, this);
        String selection = "is_music != 0 and " + MediaStore.Images.Media.DATA + " like ? ";

        ContentResolver contentResolver = getContentResolver();
        Uri songUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String orderBy = MediaStore.Audio.Media.TITLE + " ASC";
        Cursor songCursor = contentResolver.query(songUri, null, selection, new String[]{"%" + path + "%"}, orderBy);

        if (songCursor != null && songCursor.moveToFirst()) {
            int songId = songCursor.getColumnIndex(MediaStore.Audio.Media._ID);
            int songTitle = songCursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int songArtist = songCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
            int column_index = songCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);

            do {

                long currentId = songCursor.getLong(songId);
                String currentTitle = songCursor.getString(songTitle);
                String currentArtist = songCursor.getString(songArtist);
                String pathId = songCursor.getString(column_index);

                songs.add(new mag.ir.musicplayer.Models.Songs(currentId, currentTitle, currentArtist, pathId));
            } while (songCursor.moveToNext());

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(mAdapter);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    private void getSongsByArtist(String artist) {
        mAdapter = new SongAdapter(songs, this);
        String selection = "is_music != 0";
        selection = selection + " and " + MediaStore.Audio.Media.ARTIST + " = " + "'" + artist + "'";

        ContentResolver contentResolver = getContentResolver();
        Uri songUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String orderBy = MediaStore.Audio.Media.TITLE + " ASC";
        Cursor songCursor = contentResolver.query(songUri, null, selection, null, orderBy);

        if (songCursor != null && songCursor.moveToFirst()) {
            int songId = songCursor.getColumnIndex(MediaStore.Audio.Media._ID);
            int songTitle = songCursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int songArtist = songCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
            int column_index = songCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);

            do {

                long currentId = songCursor.getLong(songId);
                String currentTitle = songCursor.getString(songTitle);
                String currentArtist = songCursor.getString(songArtist);
                String pathId = songCursor.getString(column_index);

                songs.add(new mag.ir.musicplayer.Models.Songs(currentId, currentTitle, currentArtist, pathId));
            } while (songCursor.moveToNext());

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(mAdapter);
        }
    }

}
