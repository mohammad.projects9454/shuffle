package mag.ir.musicplayer.Activities;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import mag.ir.musicplayer.R;

public class MenuActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.toggleBack)
    ImageView toggleBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);

        toggleBack.setOnClickListener(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.no_change, R.anim.slide_down_info);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toggleBack:
                super.onBackPressed();
                overridePendingTransition(R.anim.no_change, R.anim.slide_down_info);
                break;
        }
    }
}
