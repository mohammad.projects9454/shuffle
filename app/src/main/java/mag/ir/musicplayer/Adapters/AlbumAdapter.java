package mag.ir.musicplayer.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mag.ir.musicplayer.Models.Albums;
import mag.ir.musicplayer.R;

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.MyViewHolder> {

    protected ItemListener mListener;
    private List<Albums> albumList;
    private Context con;
    private Typeface typeface;

    public AlbumAdapter(List<Albums> albumList, Context context, ItemListener itemListener) {
        this.con = context;
        this.albumList = albumList;
        this.mListener = itemListener;
        typeface = Typeface.createFromAsset(this.con.getAssets(), "fonts/com.ttf");
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumAdapter.MyViewHolder holder, int position) {
        holder.setData(albumList.get(position));
    }

    @NonNull
    @Override
    public AlbumAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_album, parent, false);
        return new AlbumAdapter.MyViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }

    public interface ItemListener {
        void onItemClick(Albums item);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView albumName, albumArtist, albumID;
        public ImageView albumCover;
        public CardView cv;
        public Albums album;

        public MyViewHolder(View view) {
            super(view);
            albumName = (TextView) view.findViewById(R.id.albumName);
            albumArtist = (TextView) view.findViewById(R.id.albumArtist);
            albumID = (TextView) view.findViewById(R.id.albumID);
            albumCover = (ImageView) view.findViewById(R.id.albumCover);
            cv = (CardView) view.findViewById(R.id.cv);

            itemView.setOnClickListener(this);

            albumName.setTypeface(typeface);
            albumArtist.setTypeface(typeface);
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onItemClick(album);
            }
        }

        public void setData(Albums item) {
            album = item;
            albumArtist.setText(item.getALbumArtist());
            albumName.setText(item.getAlbumName());
            albumID.setText(String.valueOf(item.getAlbumID()));

            Drawable img = Drawable.createFromPath(item.getAlbumArt());
            albumCover.setImageDrawable(img);
        }

    }
}
