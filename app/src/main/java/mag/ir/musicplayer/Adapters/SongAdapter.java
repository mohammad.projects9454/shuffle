package mag.ir.musicplayer.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.media.MediaMetadataRetriever;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import mag.ir.musicplayer.Models.Songs;
import mag.ir.musicplayer.R;

public class SongAdapter extends RecyclerView.Adapter<SongAdapter.MyViewHolder> {

    protected ItemListener mListener;
    MediaMetadataRetriever mmr = new MediaMetadataRetriever();
    private List<Songs> songList;
    private Context con;
    private Typeface typeface;

    public SongAdapter(List<Songs> songList, Context context, ItemListener itemListener) {
        this.con = context;
        this.songList = songList;
        this.mListener = itemListener;
        typeface = Typeface.createFromAsset(this.con.getAssets(), "fonts/com.ttf");
    }

    public SongAdapter(List<Songs> songList, Context context) {
        this.con = context;
        this.songList = songList;
        typeface = Typeface.createFromAsset(this.con.getAssets(), "fonts/com.ttf");
    }

    @Override
    public void onBindViewHolder(@NonNull SongAdapter.MyViewHolder holder, int position) {
        holder.setData(songList.get(position));
    }

    @NonNull
    @Override
    public SongAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_song, parent, false);
        return new SongAdapter.MyViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return songList.size();
    }

    public interface ItemListener {
        void onItemClick(Songs item);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView songName, songArtist;
        public ImageView songImage;
        public CardView cv;
        public Songs g;

        public MyViewHolder(View view) {
            super(view);
            songName = (TextView) view.findViewById(R.id.songName);
            songArtist = (TextView) view.findViewById(R.id.songArtist);
            songImage = (ImageView) view.findViewById(R.id.songImage);
            cv = (CardView) view.findViewById(R.id.cv);

            songName.setTypeface(typeface);
            songArtist.setTypeface(typeface);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onItemClick(g);
            }
        }

        public void setData(Songs songs) {
            g = songs;
            songArtist.setText(g.getmSongArtist());
            songName.setText(g.getSongTitle());
            mmr.setDataSource(g.getmPath());
            byte[] art = mmr.getEmbeddedPicture();
            if (art != null) {
                Glide.with(con).asBitmap().load(art).into(songImage);
            }
        }
    }
}
