package mag.ir.musicplayer.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import mag.ir.musicplayer.R;

public class ViewPagerAdapter extends FragmentPagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }

    public View getTabView(int position, Context context, int[] imageResId) {
        View v = LayoutInflater.from(context).inflate(R.layout.custom_tab, null);

        ImageView imgTabIcon = (ImageView) v.findViewById(R.id.imageView);
        TextView tvTabTitle = (TextView) v.findViewById(R.id.textView);

        imgTabIcon.setImageResource(imageResId[position]);
        tvTabTitle.setText(mFragmentTitleList.get(position));
        return v;
    }

    public View getTabView(int position, Context context) {
        View v = LayoutInflater.from(context).inflate(R.layout.custom_tab2, null);

        TextView tvTabTitle = (TextView) v.findViewById(R.id.textView);

        tvTabTitle.setText(mFragmentTitleList.get(position));
        return v;
    }

}