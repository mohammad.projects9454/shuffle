package mag.ir.musicplayer.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mag.ir.musicplayer.R;

public class FolderAdapter extends RecyclerView.Adapter<FolderAdapter.MyViewHolder> {

    protected ItemListener mListener;
    private List<String> folderList;
    private Context con;
    private Typeface typeface;

    public FolderAdapter(List<String> folderList, Context context, ItemListener itemListener) {
        this.con = context;
        this.folderList = folderList;
        typeface = Typeface.createFromAsset(this.con.getAssets(), "fonts/com.ttf");
        this.mListener = itemListener;
    }

    @Override
    public void onBindViewHolder(@NonNull FolderAdapter.MyViewHolder holder, int position) {
        holder.setData(folderList.get(position));
    }

    @NonNull
    @Override
    public FolderAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_folder, parent, false);
        return new FolderAdapter.MyViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return folderList.size();
    }

    public interface ItemListener {
        void onItemClick(String item);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView folderName;
        public RelativeLayout cv;
        public String string;

        public MyViewHolder(View view) {
            super(view);
            folderName = (TextView) view.findViewById(R.id.folderName);
            cv = (RelativeLayout) view.findViewById(R.id.cv);

            itemView.setOnClickListener(this);

            folderName.setTypeface(typeface);
        }

        public void setData(String g) {
            string = g;
            folderName.setText(g);
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onItemClick(string);
            }
        }
    }
}