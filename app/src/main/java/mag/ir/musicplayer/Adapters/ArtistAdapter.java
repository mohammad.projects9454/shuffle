package mag.ir.musicplayer.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mag.ir.musicplayer.Models.Artist;
import mag.ir.musicplayer.R;
import mag.ir.musicplayer.Utilities.Utility;

public class ArtistAdapter extends RecyclerView.Adapter<ArtistAdapter.MyViewHolder> {

    protected ItemListener mListener;
    private List<Artist> artistList;
    private Context con;
    private Typeface typeface;

    public ArtistAdapter(List<Artist> artistList, Context context, ItemListener itemListener) {
        this.con = context;
        this.artistList = artistList;
        typeface = Typeface.createFromAsset(this.con.getAssets(), "fonts/com.ttf");
        this.mListener = itemListener;
    }

    @Override
    public void onBindViewHolder(@NonNull ArtistAdapter.MyViewHolder holder, int position) {

        holder.setData(artistList.get(position));
    }

    @NonNull
    @Override
    public ArtistAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_artist, parent, false);
        return new ArtistAdapter.MyViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return artistList.size();
    }

    public interface ItemListener {
        void onItemClick(Artist item);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView artistName, numOfTracks, numOfAlbums;
        public CardView cv;
        public Artist g;

        public MyViewHolder(View view) {
            super(view);
            artistName = (TextView) view.findViewById(R.id.artistName);
            numOfTracks = (TextView) view.findViewById(R.id.numOfTracks);
            numOfAlbums = (TextView) view.findViewById(R.id.numOfAlbums);
            cv = (CardView) view.findViewById(R.id.cv);

            artistName.setTypeface(typeface);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onItemClick(g);
            }
        }

        public void setData(Artist artist) {
            g = artist;
            artistName.setText(g.getName());
            numOfAlbums.setText(Utility.toPersianNumber(g.getNumOfAlbums()));
            numOfTracks.setText(Utility.toPersianNumber(g.getNumOfTracks()));

        }
    }

}
