package mag.ir.musicplayer.Fragments;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mag.ir.musicplayer.Activities.InsideSongsActivity;
import mag.ir.musicplayer.Adapters.FolderAdapter;
import mag.ir.musicplayer.R;
import spencerstudios.com.bungeelib.Bungee;

public class Folders extends Fragment implements FolderAdapter.ItemListener {

    private final static String[] acceptedExtensions = {"mp3", "mp2", "wav", "flac", "ogg", "au", "snd", "mid", "midi", "kar"
            , "mga", "aif", "aiff", "aifc", "m3u", "oga", "spx"};
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private File file;
    private List<String> myList;
    private FolderAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_folders, container, false);
        ButterKnife.bind(this, v);

        myList = new ArrayList<String>();
        String root_sd = Environment.getExternalStorageDirectory().toString();
        File list[] = null;

        file = new File(root_sd);
        list = file.listFiles(new AudioFilter());

        for (int i = 0; i < list.length; i++) {
            String name = list[i].getName();
            int count = getAudioFileCount(list[i].getAbsolutePath());
            if (count != 0)
                myList.add(name);
        }

        adapter = new FolderAdapter(myList, getActivity(), this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        /*listView.setAdapter(new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, myList));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                File temp_file = new File(file, myList.get(position));
                if (!temp_file.isFile()) {
                    file = new File(file, myList.get(position));
                    File list[] = file.listFiles(new AudioFilter());
                    myList.clear();
                    for (int i = 0; i < list.length; i++) {
                        String name = list[i].getName();
                        int count = getAudioFileCount(list[i].getAbsolutePath());
                        if (count != 0)
                            myList.add(name);
                    }

                    listView.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, myList));
                }
            }
        });*/

        return v;
    }

    private int getAudioFileCount(String dirPath) {

        String selection = MediaStore.Audio.Media.DATA + " like ?";
        String[] projection = {MediaStore.Audio.Media.DATA};
        String[] selectionArgs = {dirPath + "%"};
        Cursor cursor = getActivity().managedQuery(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                projection,
                selection,
                selectionArgs,
                null);
        return cursor.getCount();
    }

    @Override
    public void onItemClick(String item) {
        Intent n = new Intent(getActivity(), InsideSongsActivity.class);
        n.putExtra("path", item);
        n.putExtra("type", "2");
        startActivity(n);
        Bungee.zoom(getActivity());
    }

    public class AudioFilter implements FileFilter {

        // only want to see the following audio file types
        private String[] extension = {".aac", ".mp3", ".wav", ".ogg", ".midi", ".3gp", ".mp4", ".m4a", ".amr", ".flac"};

        @Override
        public boolean accept(File pathname) {

            // if we are looking at a directory/file that's not hidden we want to see it so return TRUE
            if ((pathname.isDirectory() || pathname.isFile()) && !pathname.isHidden()) {
                return true;
            }

            // loops through and determines the extension of all files in the directory
            // returns TRUE to only show the audio files defined in the String[] extension array
            for (String ext : extension) {
                if (pathname.getName().toLowerCase().endsWith(ext)) {
                    return true;
                }
            }

            return false;
        }
    }

}
