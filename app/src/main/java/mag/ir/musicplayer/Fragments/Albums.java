package mag.ir.musicplayer.Fragments;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mag.ir.musicplayer.Activities.InsideSongsActivity;
import mag.ir.musicplayer.Adapters.AlbumAdapter;
import mag.ir.musicplayer.R;
import mag.ir.musicplayer.Utilities.SpacesItemDecoration;
import spencerstudios.com.bungeelib.Bungee;

public class Albums extends Fragment implements AlbumAdapter.ItemListener {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    List<mag.ir.musicplayer.Models.Albums> albumList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_albums, container, false);
        ButterKnife.bind(this, v);

        albumList = getListOfAlbums();

        GridLayoutManager manager = new GridLayoutManager(getActivity(), 2, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addItemDecoration(new SpacesItemDecoration(2, 25, true));
        AlbumAdapter adapter = new AlbumAdapter(albumList, getActivity(), this);
        recyclerView.setAdapter(adapter);

        return v;
    }

    public ArrayList<mag.ir.musicplayer.Models.Albums> getListOfAlbums() {

        final Uri uri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;
        final String _id = MediaStore.Audio.Albums._ID;
        final String album_name = MediaStore.Audio.Albums.ALBUM;
        final String artist = MediaStore.Audio.Albums.ARTIST;
        final String albumart = MediaStore.Audio.Albums.ALBUM_ART;

        final String[] columns = {_id, album_name, artist, albumart};
        Cursor cursor = getActivity().getContentResolver().query(uri, columns, null,
                null, null);

        ArrayList<mag.ir.musicplayer.Models.Albums> list = new ArrayList<mag.ir.musicplayer.Models.Albums>();

        if (cursor.moveToFirst()) {
            do {
                mag.ir.musicplayer.Models.Albums albumData = new mag.ir.musicplayer.Models.Albums();
                albumData.setAlbumID(cursor.getLong(cursor.getColumnIndex(_id)));
                albumData.setAlbumName(cursor.getString(cursor.getColumnIndex(album_name)));
                albumData.setALbumArtist(cursor.getString(cursor.getColumnIndex(artist)));
                albumData.setAlbumArt(cursor.getString(cursor.getColumnIndex(albumart)));
                list.add(albumData);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return list;
    }

    @Override
    public void onItemClick(mag.ir.musicplayer.Models.Albums item) {
        Intent n = new Intent(getActivity(), InsideSongsActivity.class);
        n.putExtra("id", item.getAlbumID() + "");
        n.putExtra("type", "1");
        startActivity(n);
        Bungee.zoom(getActivity());
    }
}
