package mag.ir.musicplayer.Fragments;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mag.ir.musicplayer.Activities.InsideSongsActivity;
import mag.ir.musicplayer.Adapters.ArtistAdapter;
import mag.ir.musicplayer.Models.Artist;
import mag.ir.musicplayer.R;
import mag.ir.musicplayer.Utilities.SpacesItemDecoration;
import spencerstudios.com.bungeelib.Bungee;

public class Artists extends Fragment implements ArtistAdapter.ItemListener {

    List<Artist> artistList;
    @BindView(R.id.rec)
    RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_songs, container, false);
        ButterKnife.bind(this, v);

        artistList = getListOfArtists();

        GridLayoutManager manager = new GridLayoutManager(getActivity(), 2, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addItemDecoration(new SpacesItemDecoration(2, 25, true));
        ArtistAdapter adapter = new ArtistAdapter(artistList, getActivity(), this);
        recyclerView.setAdapter(adapter);

        return v;
    }

    public ArrayList<Artist> getListOfArtists() {

        final Uri uri = MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI;
        final String _id = MediaStore.Audio.Artists._ID;
        final String numberOfAlbums = MediaStore.Audio.Artists.NUMBER_OF_ALBUMS;
        final String numberOfTracks = MediaStore.Audio.Artists.NUMBER_OF_TRACKS;
        final String artist = MediaStore.Audio.Artists.ARTIST;

        final String[] columns = {_id, numberOfAlbums, numberOfTracks, artist};

        Cursor cursor = getActivity().getContentResolver().query(uri, columns, null,
                null, null);

        ArrayList<Artist> list = new ArrayList<Artist>();

        if (cursor.moveToFirst()) {
            do {
                Artist albumData = new Artist();
                albumData.setId(cursor.getLong(cursor.getColumnIndex(_id)));
                albumData.setName(cursor.getString(cursor.getColumnIndex(artist)));
                albumData.setNumOfAlbums(cursor.getString(cursor.getColumnIndex(numberOfAlbums)));
                albumData.setNumOfTracks(cursor.getString(cursor.getColumnIndex(numberOfTracks)));
                list.add(albumData);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return list;
    }

    @Override
    public void onItemClick(Artist item) {
        Intent n = new Intent(getActivity(), InsideSongsActivity.class);
        n.putExtra("artist", item.getName());
        n.putExtra("type", "3");
        startActivity(n);
        Bungee.zoom(getActivity());
    }
}
