package mag.ir.musicplayer.Fragments;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.andrefrsousa.superbottomsheet.SuperBottomSheetFragment;
import com.bumptech.glide.Glide;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import mag.ir.musicplayer.Activities.PlayActivity;
import mag.ir.musicplayer.Adapters.SongAdapter;
import mag.ir.musicplayer.R;
import spencerstudios.com.bungeelib.Bungee;

public class Songs extends Fragment implements SongAdapter.ItemListener {

    public static SuperBottomSheetFragment sheet;
    @BindView(R.id.rec)
    RecyclerView recyclerView;
    private ArrayList<mag.ir.musicplayer.Models.Songs> arrayList;
    private SongAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_songs, container, false);
        ButterKnife.bind(this, v);
        arrayList = new ArrayList<mag.ir.musicplayer.Models.Songs>();

        Dexter.withActivity(getActivity())
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    getSongs();
                                }
                            });
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();


        return v;
    }

    private void getSongs() {
        mAdapter = new SongAdapter(arrayList, getActivity(), this);

        ContentResolver contentResolver = getActivity().getContentResolver();
        Uri songUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String orderBy = MediaStore.Audio.Media.TITLE + " ASC";
        Cursor songCursor = contentResolver.query(songUri, null, null, null, orderBy);

        if (songCursor != null && songCursor.moveToFirst()) {
            int songId = songCursor.getColumnIndex(MediaStore.Audio.Media._ID);
            int songTitle = songCursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int songArtist = songCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
            int column_index = songCursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);

            do {

                long currentId = songCursor.getLong(songId);
                String currentTitle = songCursor.getString(songTitle);
                String currentArtist = songCursor.getString(songArtist);
                String pathId = songCursor.getString(column_index);

                arrayList.add(new mag.ir.musicplayer.Models.Songs(currentId, currentTitle, currentArtist, pathId));
            } while (songCursor.moveToNext());

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(mAdapter);
        }
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("تقاضای دسترسی");
        builder.setMessage("نرم افزار به این دسترسی ها نیاز دارد. از تنظیمات اجازه دسترسی را بدهید");
        builder.setPositiveButton("برو به تنظیمات", (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton("لغو", (dialog, which) -> dialog.cancel());
        builder.show();
    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", "mag.ir.musicplayer", null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    @Override
    public void onItemClick(mag.ir.musicplayer.Models.Songs item) {
        sheet = new DemoBottomSheetFragment(item.getmPath(), item.getSongTitle());
        sheet.show(getActivity().getSupportFragmentManager(), "DemoBottomSheetFragment");
    }

    public static class DemoBottomSheetFragment extends SuperBottomSheetFragment {

        String path, title;
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();

        public DemoBottomSheetFragment(String path, String title) {
            this.path = path;
            this.title = title;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);

            View v = inflater.inflate(R.layout.item_song_played_bottom, container, false);

            CircleImageView cimage = v.findViewById(R.id.cover);
            TextView titlee = v.findViewById(R.id.title);
            ImageView close = v.findViewById(R.id.close);
            RelativeLayout rel = v.findViewById(R.id.rel);

            mmr.setDataSource(path);
            byte[] art = mmr.getEmbeddedPicture();
            if (art != null) {
                Glide.with(getActivity()).asBitmap().load(art).into(cimage);
            }
            spinImage(cimage);
            titlee.setText(title);

            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Songs.sheet.dismiss();
                }
            });

            rel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), PlayActivity.class);
                    startActivity(intent);
                    Bungee.zoom(getActivity());
                }
            });

            return v;
        }

        @Override
        public int getPeekHeight() {
            return 200;
        }

        @Override
        public float getDim() {
            return 0;
        }

        private void spinImage(View v) {
            RotateAnimation rotateAnimation = new RotateAnimation(0, 360f,
                    Animation.RELATIVE_TO_SELF, 0.5f,
                    Animation.RELATIVE_TO_SELF, 0.5f);

            rotateAnimation.setInterpolator(new LinearInterpolator());
            rotateAnimation.setDuration(10000);
            rotateAnimation.setRepeatCount(Animation.INFINITE);

            v.startAnimation(rotateAnimation);
        }
    }
}
